require File.expand_path(File.dirname(__FILE__) + '/../acceptance_helper')

feature "Allow members to log in one week after they are disapproved", %q{
  In order to reduce the number of questions I ask
  As a member
  I want to see a message that my account is going to be blocked in a week from now.
} do

  scenario "show the number of days left to login" do
    member = Member.make(:status => 0, :grace_period_until => 1.week.from_now)
    login(member.login)

    current_path.should == dashboard_depot_members_path
 
    page.body.should match /Je hebt nog (.*) voordat je account definitief geblokkeerd wordt./
  end
  
  scenario "grace period expired" do
    member = Member.make(:status => 0, :grace_period_until => 1.week.ago)
  
    login(member.login)
    
    current_path.should == root_path
 
    page.body.should match /Je bent afgekeurd, en kunt niet meer inloggen/
  end
end
~                                                                                                                            
~                                                                                                                            
~                                                                                                                            
~                                                                                                                            
~                                                                                                                            
